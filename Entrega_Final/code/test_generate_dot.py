import unittest
from generate_dot import generate_dot

class TestGenerateDot(unittest.TestCase):
    def test_generate_dot(self):
        states = {"right", "carry", "done"}
        transitions = {
            ("right", "1"): ("1", "R", "right"),
            ("right", "0"): ("0", "R", "right"),
            ("right", "_"): ("_", "L", "carry"),
            ("carry", "1"): ("0", "L", "carry"),
            ("carry", "0"): ("1", "L", "done"),
            ("carry", "_"): ("1", "L", "done")
        }
        dot = generate_dot(states, transitions)
        expected_dot = """digraph TuringMachine {
  rankdir=LR;
  right -> right [label="1 → 1, R"];
  right -> right [label="0 → 0, R"];
  right -> carry [label="_ → _, L"];
  carry -> carry [label="1 → 0, L"];
  carry -> done [label="0 → 1, L"];
  carry -> done [label="_ → 1, L"];
}
"""
        self.assertEqual(dot, expected_dot)

if __name__ == '__main__':
    unittest.main()