import unittest
from turing_machine import TuringMachine

class TestTuringMachine(unittest.TestCase):
    def setUp(self):
        self.states = {"right", "carry", "done"}
        self.alphabet = {"0", "1", "_"}
        self.initial_state = "right"
        self.blank_symbol = "_"
        self.transitions = {
            ("right", "1"): ("1", "R", "right"),
            ("right", "0"): ("0", "R", "right"),
            ("right", "_"): ("_", "L", "carry"),
            ("carry", "1"): ("0", "L", "carry"),
            ("carry", "0"): ("1", "L", "done"),
            ("carry", "_"): ("1", "L", "done")
        }
        self.tm = TuringMachine(self.states, self.alphabet, self.initial_state, self.blank_symbol, self.transitions)

    def test_run(self):
        result = self.tm.run("1100")
        self.assertEqual(result, "1101")

    def test_generate_dot(self):
        dot = self.tm.generate_dot()
        expected_dot = """digraph TuringMachine {
  rankdir=LR;
  right -> right [label="1 → 1, R"];
  right -> right [label="0 → 0, R"];
  right -> carry [label="_ → _, L"];
  carry -> carry [label="1 → 0, L"];
  carry -> done [label="0 → 1, L"];
  carry -> done [label="_ → 1, L"];
}
"""
        self.assertEqual(dot, expected_dot)

if __name__ == '__main__':
    unittest.main()