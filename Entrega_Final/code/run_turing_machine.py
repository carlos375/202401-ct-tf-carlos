from turing_machine import TuringMachine

states = {"right", "carry", "done"}
alphabet = {"0", "1", "_"}
initial_state = "right"
blank_symbol = "_"
transitions = {
    ("right", "1"): ("1", "R", "right"),
    ("right", "0"): ("0", "R", "right"),
    ("right", "_"): ("_", "L", "carry"),
    ("carry", "1"): ("0", "L", "carry"),
    ("carry", "0"): ("1", "L", "done"),
    ("carry", "_"): ("1", "L", "done")
}

tm = TuringMachine(states, alphabet, initial_state, blank_symbol, transitions)
result = tm.run("1100")
print("Estado final de la cinta:", result)

tm.visualize("turing_machine")