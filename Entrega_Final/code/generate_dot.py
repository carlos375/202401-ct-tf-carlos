def generate_dot(states, transitions):
    dot_representation = "digraph TuringMachine {\n"
    dot_representation += "  rankdir=LR;\n"
    
    for (state, symbol), (new_symbol, direction, new_state) in transitions.items():
        dot_representation += f'  {state} -> {new_state} [label="{symbol} → {new_symbol}, {direction}"];\n'
    
    dot_representation += "}\n"
    return dot_representation

if __name__ == "__main__":
    states = {"right", "carry", "done"}
    transitions = {
        ("right", "1"): ("1", "R", "right"),
        ("right", "0"): ("0", "R", "right"),
        ("right", "_"): ("_", "L", "carry"),
        ("carry", "1"): ("0", "L", "carry"),
        ("carry", "0"): ("1", "L", "done"),
        ("carry", "_"): ("1", "L", "done")
    }
    
    dot = generate_dot(states, transitions)
    with open("turing_machine.dot", "w", encoding="utf-8") as file:
        file.write(dot)