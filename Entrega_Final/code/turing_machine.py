from graphviz import Source

class TuringMachine:
    def __init__(self, states, alphabet, initial_state, blank_symbol, transitions):
        self.states = states
        self.alphabet = alphabet
        self.initial_state = initial_state
        self.blank_symbol = blank_symbol
        self.transitions = transitions
        self.tape = {}
        self.head_position = 0
        self.current_state = initial_state

    def step(self):
        current_symbol = self.tape.get(self.head_position, self.blank_symbol)
        action = self.transitions.get((self.current_state, current_symbol))

        if not action:
            raise Exception(f"No transition defined for state {self.current_state} and symbol {current_symbol}")

        new_symbol, move_direction, new_state = action
        self.tape[self.head_position] = new_symbol
        self.current_state = new_state

        if move_direction == "R":
            self.head_position += 1
        elif move_direction == "L":
            self.head_position -= 1

    def run(self, input_string):
        for i, char in enumerate(input_string):
            self.tape[i] = char

        while self.current_state not in {"halt", "done"}:
            self.step()

        return self.get_tape_contents()

    def get_tape_contents(self):
        min_used_index = min(self.tape.keys())
        max_used_index = max(self.tape.keys())
        tape_contents = "".join(self.tape.get(i, self.blank_symbol) for i in range(min_used_index, max_used_index + 1))
        return tape_contents.rstrip(self.blank_symbol)  # Elimina símbolos en blanco al final

    def generate_dot(self):
        dot_representation = "digraph TuringMachine {\n"
        dot_representation += "  rankdir=LR;\n"
        
        for (state, symbol), (new_symbol, direction, new_state) in self.transitions.items():
            dot_representation += f'  {state} -> {new_state} [label="{symbol} → {new_symbol}, {direction}"];\n'
        
        dot_representation += "}\n"
        return dot_representation

    def visualize(self, filename="turing_machine"):
        dot_code = self.generate_dot()
        graph = Source(dot_code)
        graph.render(filename, format='png', cleanup=True)
        print(f"Graph generated and saved as {filename}.png")