v# Informe del Proyecto: Máquina de Turing

## Introducción

La máquina de Turing es un modelo computacional fundamental que sirve como base para la teoría de la computación. Este proyecto tiene como objetivo implementar una aplicación que permita compilar y ejecutar máquinas de Turing, proporcionando una herramienta educativa y de investigación en el campo de los compiladores.

## Objetivos

- Definir un lenguaje de representación para las máquinas de Turing.
- Implementar un compilador que ejecute estas máquinas y genere su estado final.
- Permitir la ejecución JIT de las máquinas de Turing.
- Generar representaciones gráficas en lenguaje DOT para visualización con Graphviz.

## Marco Teórico

### La Máquina de Turing

La máquina de Turing, concebida por Alan Turing en 1936, es un dispositivo teórico que manipula símbolos en una tira de cinta de acuerdo a una tabla de reglas. A pesar de su simplicidad, puede simular la lógica de cualquier algoritmo de computación, siendo un modelo de referencia para la computabilidad.

## Metodología

1. **Definición del Lenguaje de Representación:**
   Se definió un conjunto de reglas para representar estados, símbolos y transiciones de la máquina de Turing.

2. **Implementación en Python:**
   Se desarrollaron clases y funciones en Python para simular el comportamiento de la máquina de Turing y permitir su ejecución JIT.

3. **Generación de Grafo en DOT:**
   Se implementó un script para convertir la representación de la máquina de Turing a un grafo en lenguaje DOT.

4. **Pruebas y Validación:**
   Se realizaron pruebas unitarias para asegurar el correcto funcionamiento de la simulación y la generación del grafo.

## Conclusiones

El proyecto demostró ser efectivo para la simulación de máquinas de Turing y la generación de sus representaciones gráficas. La implementación en Python permite una ejecución flexible y la visualización en Graphviz facilita la comprensión de la estructura y funcionamiento de las máquinas de Turing.

## Referencias

- Turing, A. M. (1936). On Computable Numbers, with an Application to the Entscheidungsproblem.
- [Graphviz Documentation](https://graphviz.org/doc/info/lang.html)
- [Turing Machine Simulator](https://turingmachine.io)