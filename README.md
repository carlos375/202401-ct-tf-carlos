# Teoría de Compiladores
##### Trabajo Final

## Instrucciones

* Haga un **fork** de este repositorio y resuelva el proyecto allí.
* Su solución debe estar dentro de una carpeta cuyo nombre sera su nombre de grupo.
* El informe será un archivo llamado `README.md` dentro de su carpeta.
* El trabajo se desarrolla en grupos de 3 personas los cuales estan registradas en el AV.

## Enunciado

El trabajo final consiste en implementar una aplicación que permita compilar y ejecutar maquinas Turing.
![Turing Machine](assets/turingmachine01.png)
Usted deberá crear su propia manera de definir una máquina de turing como código fuente el cual será el objeto del proyecto.

## Tareas
* Definir el lenguaje de representación de una máquina de Turing, puede usar o tomar como base la que se describe en [el siguiente enlace](https://turingmachine.io).
* Debe permitir generar un ejecutable de la máquina de turing que al finalizar imprima el estado final de la cinta.
* Debe también permitir la ejecución JIT.
* Debe permitir generar la máquina de turing como un grafo en el lenguaje [DOT de graphviz](https://graphviz.org/doc/info/lang.html)
* Debe elaborar un informe conteniendo los siguientes puntos:
  1. Introducción: problema, motivación, solución propuesta
  2. Objetivos
  3. Marco teórico
  4. Metodologíá: el proceso paso a paso que siguió para desarrollar el proyecto.
  5. Conclusiones
  6. Referencias.

## Rúbrica de evaluación

Item | Sobresaliente | Esperado | Deficiente
---|---|---|---
Definición del lenguaje | (2 puntos) El lenguaje para definir la máquina de turing es completa y permite definir cualquier máquina de turing, permite definir configuración de la máquina como el input, tamaño de la cinta, alfabeto, etc. | (1 punto) El lenguaje es básico permite definir ciertos tipos de máquinas con limitaciones | (0 puntos) No implementado
Frontend | (4 puntos)  | (2 puntos)  | (0 puntos) No implementado
Generación de IR | (3 puntos)  | (1.5 puntos)  | (0 puntos) No implementado
Optimización | (3 puntos)  | (1.5 punto)  | (0 puntos) No implementado
Generación de ejecutable | (2 puntos)  | (1 punto)  | (0 puntos) No implementado
Ejecución JIT | (2 puntos)  | (1 punto)  | (0 puntos) No implementado
Generación de DOT | (2 punto)  | (1 punto)  | (0 puntos) No implementado
Presentación | (2 puntos) Elabora el informe correctamente, un PPT adecuado y un Video demostrativo de 1 a 2 minutos | (1 punto) Informe deficiente, video incompleto | (0 puntos) No implementado